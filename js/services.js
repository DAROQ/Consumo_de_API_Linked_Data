(function(){
   'use strict';   
   angular.module('linked.services',[])
   
   .factory('DBpediaService',['$resource',function($resource){
      return $resource('http://dbpedia.org/sparql');
   }]);
   
})();